'use strict';

var UserModel = require('../models/userModel.js');

exports.list_all_users = function(req, res) {
  UserModel.getAllUser(function(err, user) {
    if (err){
      res.status(400).send({ error: true, message: err.sqlMessage });
    }else{
      res.status(200).send({ error: false, data: user, message: 'Listado de usuarios.' });
    }
  });
};

exports.read_a_user = function(req, res) {
  UserModel.getUserById(req.params.userId, function(err, user) {
    if (err){
      res.status(400).send({ error: true, message: err.sqlMessage });
    }else{
      res.status(200).send({ error: false, data: user, message: 'Datos del usuario.' });
    }
  });
};

exports.create_a_user = function(req, res) {
  var new_user = new UserModel(req.body);
  if(!new_user.name || !new_user.email){
    res.status(400).send({ error:true, message: 'Por favor envie el nombre y el correo del usuario.' });
  }else{
    UserModel.createUser(new_user, function(err, user) {
      if (err){
        res.status(400).send({ error: true, message: err.sqlMessage });
      }else{
        res.status(200).send({ error: false, data: user.insertId, message: 'El usuario se ha creado correctamente.' });
      }
    });
  }
};

exports.delete_a_user = function(req, res) {
  UserModel.remove(req.params.userId, function(err, user) {
    if (err){
      res.status(400).send({ error: true, message: err.sqlMessage });
    }else{
      res.status(200).send({ error: false, message: 'Usuario eliminado correctamente.' });
    }
  });
};

exports.update_a_user = function(req, res) {
  if(!req.params.userId){
    res.status(400).send({ error:true, message: 'Por favor envie el id del usuario.' });
  }else{
    UserModel.updateUser(req.params.userId, req.body, function(err, user) {
      if (err){
        res.status(400).send({ error: true, message: err.sqlMessage });
      }else{
        res.status(200).send({ error: false, message: 'El usuario se ha actualizado correctamente.' });
      }
    });
  }
};