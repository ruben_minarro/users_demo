'user strict';
var sql = require('../../db.js');

var User = function(user){
  this.name = user.name;
  this.email = user.email;
  this.created_at = new Date();
};

User.getAllUser = function (result) {
  sql.query("SELECT * FROM users ORDER BY name ASC", function (err, res) {
    if(err) {
      result(err, null);
    }else{
      result(null, res);
    }
  });   
};

User.createUser = function (newUser, result) {    
  sql.query("INSERT INTO users SET ?", newUser, function (err, res) {
    if(err) {
      result(err, null);
    }else{
      result(null, res);
    }
  });
};

User.getUserById = function (userId, result) {
  sql.query("SELECT * FROM users WHERE id = ? ", userId, function (err, res) {             
    if(err) {
      result(err, null);
    }else{
      result(null, res);
    }
  });   
};

User.updateUser = function(userId,user,result){
  sql.query("UPDATE users SET ? WHERE id = ?", [user,userId], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{   
      result(null, res);
    }
  });
};

User.remove = function(id, result){
  sql.query("DELETE FROM users WHERE id = ?", [id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{
      result(null, res);
    }
  }); 
};

module.exports= User;